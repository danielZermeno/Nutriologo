﻿$(document).ready(function () {
    var d = new Date();

    var month = d.getMonth() + 1;
    var day = d.getDate();

    var output = d.getFullYear() + '-' +
        (('' + month).length < 2 ? '0' : '') + month + '-' +
        (('' + day).length < 2 ? '0' : '') + day;

    $("#Fecha").on("change",function () {
        if (Date.parse($("#Fecha").val()) < Date.parse(output) || $("#Fecha").val() === "") {
            alert("Favor de escribir una Fecha valida");
            $("#Fecha").val("");
            $("#RegistrarCita").attr("disabled", true);
        } else {
            $("#RegistrarCita").removeAttr("disabled");
            if ($("#Fecha").val() !== "" && $("#Hora").val() !== "")
                ValidarCita();
        }
    })

    $("#Hora").on("change", function () {
        if($("#Fecha").val() !== "" && $("#Hora").val() !== "" )
        ValidarCita();
    });
});

function ValidarCita() {
    Fecha= {
        dia: $("#Fecha").val(),
        hora: $("#Hora").val()
    }
    $.ajax({
        type: "POST",
        url: "../Citas/ValidarFecha",
        data: Fecha,
        success: function (result) {
            console.log("Si funcione")
            console.log(result);
            if (!result.estatus) {
                alert(result.mensaje);
                $("#RegistrarCita").attr("disabled", true);
                $("#Hora").val("");
            } else {
                $("#RegistrarCita").removeAttr("disabled");
            }
        },
        error: function (err) {
            console.log("No funcione");
            console.log(err);
        }

    })
}
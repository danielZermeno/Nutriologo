﻿$(document).ready(function () {
    $("#CitasActuales").hide();
    $("#CitasAnterioresSeccion").hide();
    $("#TituloAnterior").hide();
    $("#CitasAnteriores").click(function () {
        $("#CitasActualesSeccion").removeClass("col-sm-12 col-md-12 col-lg-12 col-xl-12");
        $("#CitasActualesSeccion").addClass("col-sm-6 col-md-6 col-lg-6 col-xl-6");

        $("#TituloActual").removeClass("col-sm-12 col-md-12 col-lg-12 col-xl-12");
        $("#TituloActual").addClass("col-sm-6 col-md-6 col-lg-6 col-xl-6");

        $("#TituloAnterior").show(500);
        $("#CitasAnterioresSeccion").show(500);
        $("#CitasAnteriores").hide();
        $("#CitasActuales").show();
    });
    $("#CitasActuales").click(function () {
        $("#CitasActualesSeccion").removeClass("col-sm-6 col-md-6 col-lg-6 col-xl-6");
        $("#CitasActualesSeccion").addClass("col-sm-12 col-md-12 col-lg-12 col-xl-12");
        $("#CitasAnterioresSeccion").hide();

        $("#TituloActual").removeClass("col-sm-6 col-md-6 col-lg-6 col-xl-6");
        $("#TituloActual").addClass("col-sm-12 col-md-12 col-lg-12 col-xl-12");
        $("#TituloAnterior").hide();

        $("#CitasAnteriores").show();
        $("#CitasActuales").hide();
    });
});
﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;


namespace Nutriologo.Controllers
{
    [Route("[controller]/[action]")]
    public class AccountController : Controller
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationDbContext _context;
        //private readonly RoleManager<IdentityRole> _roleManager;
        ApplicationUser usuario = null;

        public AccountController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, ApplicationDbContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }
        
        //Get Account/Register
        public IActionResult Register(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        //POST Account/Register
        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel registerViewModel)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser
                {
                    Nombre = registerViewModel.Nombre,
                    ApellidoMaterno = registerViewModel.ApellidoMaterno,
                    ApellidoPaterno = registerViewModel.ApellidoPaterno,
                    Email = registerViewModel.Email,
                    Password = registerViewModel.Password,
                    UserName = registerViewModel.Email,
                    IdDatosPrivados = 0,
                    EmailConfirmed = true
                };
                try
                {
                    var result = await _userManager.CreateAsync(user, registerViewModel.Password);

                    if (result.Succeeded == false)
                    {
                        return View(registerViewModel);
                    }

                    //Agregamos el Rol al usuario
                    await _userManager.AddToRoleAsync(user, "Usuario");

                    //Logiamos al usuario
                    var login = await _signInManager.PasswordSignInAsync(user.Email, user.Password, false, false);

                    if (login.Succeeded)
                    {
                        return RedirectToAction("CompletarRegistro","Account");
                    }
                    else
                    {
                        return RedirectToAction("Login","Account");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
            return View(registerViewModel);
        }

        //Get Account/Login
        public IActionResult Login()
        {
            if (User.IsInRole("Usuario") || User.IsInRole("Admin"))
                return RedirectToAction("Consultar", "Dashboard");
            else
            {
                return View();
            }
        }

        //POST Account/Login
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel login)
        {
            if (ModelState.IsValid)
            {
                var resultado = await _signInManager.PasswordSignInAsync(login.Email, login.Password, false, lockoutOnFailure: false);
                if (resultado.IsNotAllowed)
                {
                    return View(login);
                }

                if (resultado.Succeeded)
                {
                    // Resolve the user via their email
                    var user = await _userManager.FindByEmailAsync(login.Email);
                    // Get the roles for the user
                    var roles = await _userManager.GetRolesAsync(user);
                    if (roles[0] == "Admin")
                    {
                        return RedirectToAction("Ver", "Citas");
                    }
                    else
                    {
                        return RedirectToAction("Consultar", "Dashboard");
                    }
                }
            }
            return View(login);
        }

        //GET Account/DatosPrivados
        public async Task<IActionResult> CompletarRegistro()
        {
            usuario =await _userManager.FindByEmailAsync(User.Identity.Name);
            if(usuario.IdDatosPrivados != 0)
            {
                usuario.DatosPrivados = await _context.DatosPrivados.SingleOrDefaultAsync(dp => dp.Id == usuario.IdDatosPrivados);
                return View(usuario.DatosPrivados);
            }
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CompletarRegistro (DatosPrivados datosPrivados)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var usr = User.Identity.Name;
                    usuario = await _userManager.FindByEmailAsync(usr);

                    if(usuario.IdDatosPrivados != 0)
                    {
                        _context.DatosPrivados.Update(datosPrivados);
                        await _context.SaveChangesAsync();
                    }
                    else
                    {

                        _context.DatosPrivados.Add(datosPrivados);
                        await _context.SaveChangesAsync();

                        usuario.IdDatosPrivados = datosPrivados.Id;

                        await _userManager.UpdateAsync(usuario);
                    }


                    return RedirectToAction("Consultar", "Dashboard");
                }
                catch(Exception e)
                {
                    Console.WriteLine(e);
                }
            }
            return View(datosPrivados);
        }

        public async Task<IActionResult> VerUsuarios()
        {
            List<ApplicationUser> users = await _context.Users.ToListAsync();
            return View(users);
        }

        public async Task<IActionResult> VerPaciente(string id)
        {
            ApplicationUser paciente = await _context.Users.SingleOrDefaultAsync(u => u.Id == id);
            paciente.consultas = await _context.Consultas.Where(c => c.Correo == paciente.Email).ToListAsync();
            return View(paciente);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Nutriologo;
using Nutriologo.Models;

namespace Nutriologo.Controllers
{
    public class CitasController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        ApplicationUser usuario;

        public CitasController(ApplicationDbContext context, UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        // GET: Citas
        public async Task<IActionResult> Registrar()
        {
            Cita cita = new Cita();
            usuario = await _userManager.FindByEmailAsync(User.Identity.Name);
            if (User.IsInRole("Usuario"))
            {
                cita.Nombre = usuario.Nombre;
                cita.ApellidoPaterno = usuario.ApellidoPaterno;
                cita.ApellidoMaterno = usuario.ApellidoMaterno;
                cita.Email = usuario.Email;
                try
                {
                    usuario.DatosPrivados =await _context.DatosPrivados.SingleOrDefaultAsync(dp => dp.Id == usuario.IdDatosPrivados);
                }
                catch(Exception e)
                {
                    Console.Write(e);
                }
                cita.Telefono = usuario.DatosPrivados.TelefonoCelular;
            }
            return PartialView(cita);
        }

        [HttpPost]
        public async Task<IActionResult> Registrar(Cita cita)
        {
            if (ModelState.IsValid)
            {
                if (User.IsInRole("Usuario"))
                {
                    usuario = await _userManager.FindByEmailAsync(User.Identity.Name);
                    cita.Email = usuario.Email;

                    _context.Add(cita);
                    await _context.SaveChangesAsync();
                }
                else
                {

                }
                return RedirectToAction("Ver", "Citas");
            }
            return View(cita);
        }


        [HttpPost]
        public async Task<EstatusJson> ValidarFecha(DateTime Dia, DateTime Hora)
        {
            EstatusJson estatusJson = new EstatusJson
            {
                Estatus = false,
                Mensaje = "El espacio ya esta ocupado"
            };
            var cita = await _context.Citas.SingleOrDefaultAsync(c => c.Fecha.ToShortDateString() == Dia.ToShortDateString() && c.Hora.ToShortTimeString() == Hora.ToShortTimeString() );
            if (cita == null)
            {
                estatusJson.Mensaje = "Se puede registrar la cita";
                estatusJson.Estatus = true;
            }
            return estatusJson;
        }

        // GET: Citas/Details/5
        public async Task<IActionResult> Ver()
        {
            usuario = await _userManager.FindByEmailAsync(User.Identity.Name);

            List<Cita> citas = new List<Cita>();

            if (User.IsInRole("Usuario"))
            {
                citas = await _context.Citas.Where(c => c.Email == usuario.Email).ToListAsync();
            }
            else
            {
                citas = await _context.Citas.Where(c => c.Fecha.ToShortDateString() == DateTime.Now.ToShortDateString() && c.FueConsultada == false).ToListAsync();
            }
            return View(citas);
        }

        // GET: Citas/Create
        public async Task<IActionResult> Consultar(int id)
        {
            Consulta consulta = new Consulta();
            if(id != 0)
            {
                consulta.cita = await _context.Citas.SingleOrDefaultAsync(c => c.Id == id);
                consulta.consultas = await _context.Consultas.Where(c => c.Fecha.Date < DateTime.Now.Date && c.Correo == consulta.cita.Email).Take(5).ToListAsync();
            }
            return View(consulta);
        }

        [HttpPost]
        public async Task<IActionResult> Consultar(Consulta consulta)
        {
            //consulta.Fecha = DateTime.Now;
            if (ModelState.IsValid)
            {
                consulta.Fecha = DateTime.Now;
                consulta.Id = 0;
                _context.Consultas.Add(consulta);
                await _context.SaveChangesAsync();

                return RedirectToAction("Ver", "Citas");
            }
            try
            {
                consulta.cita = await _context.Citas.SingleOrDefaultAsync(c => c.Id == consulta.cita.Id);
                consulta.consultas = await _context.Consultas.Where(c => c.Fecha.Date < DateTime.Now.Date).Take(5).ToListAsync();
            }
            catch(Exception e)
            {
                Console.Write(e);
            }
            return View(consulta);
        }

        // GET: Citas/Edit/5
        public async Task<IActionResult> Modificar(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cita = await _context.Citas.FindAsync(id);
            if (cita == null)
            {
                return NotFound();
            }
            return View(cita);
        }

        // POST: Citas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Modificar( Cita cita)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(cita);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CitaExists(cita.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Ver","Citas");
            }
            return View(cita);
        }

        // GET: Citas/Delete/5
        public async Task<IActionResult> Delete(int id)
        {
            if (id == 0)
            {
                return NotFound();
            }

            var cita = await _context.Citas
                .FirstOrDefaultAsync(m => m.Id == id);
            if (cita == null)
            {
                return NotFound();
            }

            _context.Citas.Remove(cita);
            await _context.SaveChangesAsync();

            return RedirectToAction("Ver", "Citas");
        }

        private bool CitaExists(int id)
        {
            return _context.Citas.Any(e => e.Id == id);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Nutriologo.Models;

namespace Nutriologo.Controllers
{
    public class ConsultaController : Controller
    {

        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        ApplicationUser usuario;

        public ConsultaController(ApplicationDbContext context, UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        // GET: Consulta
        public async Task<IActionResult> Index()
        {
            usuario = await _userManager.FindByEmailAsync(User.Identity.Name);
            List<Consulta> consultas = await(from co in _context.Consultas
                                             where co.FacturaDisponible == true && co.Correo == usuario.Email
                                            select co
                                            ).ToListAsync();

            return View(consultas);
        }

        // GET: Consulta/Create
        public async Task<IActionResult> Registrar(string correo, DateTime hora)
        {

            Cita cita = await (from ci in _context.Citas
                                 where ci.Email == correo && ci.Hora == hora
                                select ci
                                ).FirstOrDefaultAsync();

            Consulta consulta = new Consulta();

            cita.FueConsultada = true;
            consulta.Correo = cita.Email;
            consulta.Fecha = cita.Fecha;
            consulta.FacturaDisponible = true;
            consulta.IdCita = cita.Id;

            _context.Update(cita);
            await _context.SaveChangesAsync();

            return View(consulta);
        }

        // POST: Consulta/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Registrar(IFormCollection collection)
        {
            try
            {

                Consulta consulta = new Consulta();
                consulta.Correo = collection["Correo"];
                consulta.Costo = float.Parse(collection["Costo"]);
                consulta.FacturaDisponible = true;
                consulta.Fecha = DateTime.Parse(collection["Fecha"]);
                consulta.Peso = float.Parse(collection["Peso"]);
                consulta.Talla = float.Parse(collection["Talla"]);
                consulta.IdCita = int.Parse(collection["IdCita"]);

                _context.Consultas.Add(consulta);

                _context.SaveChanges();

                return RedirectToAction("Ver", "Citas");
            }
            catch
            {
                return View();
            }
        }

        // GET: Consulta/Edit/5
        public ActionResult Editar(int id)
        {
            return View();
        }

        // POST: Consulta/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editar(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Consulta/Delete/5
        public ActionResult Eliminar(int id)
        {
            return View();
        }

        // POST: Consulta/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Eliminar(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
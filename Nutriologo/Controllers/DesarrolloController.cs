﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Mvc;

namespace Nutriologo.Controllers
{
    public class DesarrolloController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly RoleManager<IdentityRole> _roleManager;
        //private readonly UserManager<ApplicationUser> _userManager;

        public DesarrolloController(
                        ApplicationDbContext context,
                        RoleManager<IdentityRole> roleManager
                       // UserManager<ApplicationUser> userManager
            )
        {
            _context = context;
            _roleManager = roleManager;
            //_userManager = userManager;
        }


        private async Task CrearRoles()
        {
            if (!await _roleManager.RoleExistsAsync("Admin"))
                await _roleManager.CreateAsync(new IdentityRole { Name = "Admin" });
            if (!await _roleManager.RoleExistsAsync("Usuario"))
                await _roleManager.CreateAsync(new IdentityRole { Name = "Usuario" });
        }

        public async Task<IActionResult> InicializarAplicacion()
        {
            await CrearRoles();
            return RedirectToAction("Index", "Home");
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Nutriologo;
using Nutriologo.Models;

namespace Nutriologo.Controllers
{
    public class FacturasController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        ApplicationUser usuario;

        public FacturasController(ApplicationDbContext context, UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        // GET: Facturas1
        public async Task<IActionResult> Ver()
        {
            usuario = await _userManager.FindByEmailAsync(User.Identity.Name);

            List<Cita> citas = new List<Cita>();
            citas = await _context.Citas.Where(c => c.Email == usuario.Email).ToListAsync();

            List < Factura >facturas = new List<Factura>();
            if (citas.Count > 0)
            {
                foreach(var item in citas)
                {
                    var factura = await _context.Facturas.Where(f => f.IdConsulta == item.Id).SingleOrDefaultAsync();
                    if(factura != null)
                    {

                    facturas.Add(factura);
                    }
                }
            }
            return View(facturas);
        }

        // GET: Facturas1/Details/5
        public async Task<IActionResult> Details()
        {
            usuario = await _userManager.FindByEmailAsync(User.Identity.Name);

            Factura factura = new Factura();

            factura.correo = usuario.Email;

            return View(factura);
        }

        // GET: Facturas1/Create
        public async Task<IActionResult> Create(int id)
        {

            Consulta consulta = await(from co in _context.Consultas
                              where co.IdCita == id
                              select co
                                ).FirstOrDefaultAsync();

            consulta.FacturaDisponible = false;

            Factura factura = new Factura();

            factura.IdConsulta = consulta.IdCita;

            _context.Update(consulta);
            await _context.SaveChangesAsync();

            return View(factura);
        }

        // POST: Facturas1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(IFormCollection collection)
        {
            try
            {
                usuario = await _userManager.FindByEmailAsync(User.Identity.Name);

                Factura factura = new Factura();

                factura.FechaFactura = DateTime.Now;
                factura.RFCCliente = collection["RFCCliente"];
                factura.IdConsulta = int.Parse(collection["IdConsulta"]);
                factura.correo = usuario.Email;

                _context.Facturas.Add(factura);

                _context.SaveChanges();

                return RedirectToAction("Details", "Facturas");
            }
            catch
            {
                return View();
            }
        }

        // GET: Facturas1/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var factura = await _context.Facturas.FindAsync(id);
            if (factura == null)
            {
                return NotFound();
            }
            return View(factura);
        }

        // POST: Facturas1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,IdConsulta,FechaFactura,RFCCliente")] Factura factura)
        {
            if (id != factura.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(factura);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FacturaExists(factura.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Ver));
            }
            return View(factura);
        }

        // GET: Facturas1/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var factura = await _context.Facturas
                .FirstOrDefaultAsync(m => m.Id == id);
            if (factura == null)
            {
                return NotFound();
            }

            return View(factura);
        }

        // POST: Facturas1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var factura = await _context.Facturas.FindAsync(id);
            _context.Facturas.Remove(factura);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Ver));
        }

        private bool FacturaExists(int id)
        {
            return _context.Facturas.Any(e => e.Id == id);
        }
    }
}

﻿using Microsoft.AspNetCore.Identity;
using Nutriologo.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Nutriologo
{
    [Table("Usuario")]
    public class ApplicationUser : IdentityUser
    {

        [Required]
        [MaxLength(20)]
        public string Nombre { get; set; }

        [Required]
        [MaxLength(20)]
        public string ApellidoPaterno { get; set; }

        [Required]
        [MaxLength(20)]
        public string ApellidoMaterno { get; set; }

        [Required]
        public int IdDatosPrivados { get; set; }

        [NotMapped]
        public DatosPrivados DatosPrivados { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [NotMapped]
        public List<Consulta> consultas { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Nutriologo.Models
{
    public class Consulta
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        public int IdCita { get; set; }

        [Required(ErrorMessage = "Es necesario el Peso")]
        public float Peso { get; set; }

        [Required(ErrorMessage = "Es necesario la Talla")]
        public float Talla { get; set; }
        
        
        public DateTime Fecha { get; set; }

        
        public string Correo { get; set; }

        public bool FacturaDisponible { get; set; }

        public float Costo { get; set; }

        [NotMapped]
        public List<Consulta> consultas { get; set; }

        [NotMapped]
        public Cita cita { get; set; }
    }
}

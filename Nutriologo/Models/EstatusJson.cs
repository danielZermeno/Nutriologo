﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nutriologo.Models
{
    public class EstatusJson
    {
        public bool Estatus { get; set; }
        public string Mensaje { get; set; }
        public Object objeto { get; set; }
    }
}

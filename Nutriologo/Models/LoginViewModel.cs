﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Nutriologo
{
    public class LoginViewModel
    {
        [Required (ErrorMessage = "El campo Correo es necesario")]
        [EmailAddress]
        [Display(Name = "Correo")]
        public string Email { get; set; }

        [Required (ErrorMessage = "El campo Contraseña es requerido")]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }
    }
}

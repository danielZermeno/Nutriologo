﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Nutriologo
{
    public class DatosPrivados
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        [Required (ErrorMessage = "El campo Calle es requerido")]
        [Display(Name = "Calle")]
        public string Calle { get; set; }

        [Required (ErrorMessage = "El campo Colonia es requerido")]
        [Display(Name = "Colonia")]
        public string Colonia { get; set; }

        [Required (ErrorMessage = "El campo Estado es requerido")]
        [Display(Name = "Estado")]
        public string Estado { get; set; }

        [Required (ErrorMessage = "El campo Municipio es requerido")]
        [Display(Name = "Municipio")]
        public string Municipio { get; set; }

        [Required (ErrorMessage = "El campo Numero Exterior es requerido")]
        [Display(Name = "Numero Exterior")]
        public string NumExt { get; set; }

        //[Required (ErrorMessage = "El campo Numero Interior es requerido")]
        [Display (Name = "Numero Interior")]
        public string NumInt { get; set; }

        [Required (ErrorMessage = "El campo Telefono Celular es requerido")]
        [Display(Name = "Telefono Celular")]
        [StringLength(10, ErrorMessage = "El campo Telefono Celular debe tener 10 caracteres")]
        public string TelefonoCelular { get; set; }

        [Display(Name = "Es Whatsapp?")]
        public bool TelefonoCelularEsWhats { get; set; }
    }
}

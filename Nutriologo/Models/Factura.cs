﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Nutriologo.Models
{
    public class Factura
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        public int IdConsulta { get; set; }

        public DateTime FechaFactura { get; set; }

        public string RFCCliente { get; set; }

        public string correo { get; set; }

        [NotMapped]
        public Consulta consulta { get; set; }
    }
}

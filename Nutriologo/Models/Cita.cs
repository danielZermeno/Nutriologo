﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Nutriologo.Models
{
    public class Cita
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        public int IdUsuario { get; set; }

        [Required(ErrorMessage = "La Fecha es requerida")]
        [Display(Name = "Fecha")]
        public DateTime Fecha { get; set; }

        [Required(ErrorMessage = "La Hora es requerida")]
        [Display(Name = "Hora")]
        [DataType(DataType.DateTime)]
        public DateTime Hora { get; set; }

        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "El campo Nombre es requerido")]
        public string Nombre { get; set; }

        [Display(Name = "Apellido Materno")]
        [Required(ErrorMessage = "El campo Apellido Materno es requerido")]
        public string ApellidoMaterno { get; set; }

        [Display(Name = "Apellido Paterno")]
        [Required(ErrorMessage = "El campo Apellido Paterno es requerido")]
        public string ApellidoPaterno { get; set; }

        [Display(Name = "Telefono")]
        [Required(ErrorMessage = "El Campo Telefono es requerido")]
        public string Telefono { get; set; }

        [Display(Name = "Correo")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public bool FueConsultada { get; set; }
    }
}

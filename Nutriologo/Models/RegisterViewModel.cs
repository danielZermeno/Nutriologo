﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Nutriologo
{
    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "La {0} debe tener minimo {2} y maximo {1} caracteres de longitud.", MinimumLength = 6)]
        [RegularExpression("^.*(?=.{6,})(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[.@#$%^&+=]).*$", ErrorMessage = "La contraseña debe contener al menos 6 caracteres una letra Mayuscula, Minuscula y un Caracter Especial (.@#$%^&+=)")]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmar Contraseña")]
        [Compare("Password", ErrorMessage = "La Contraseña y la Confirmación no coinciden.")]
        public string ConfirmPassword { get; set; }

        [Required (ErrorMessage = "El campo Nombre es requerido")]
        [Display(Name = "Nombre")]
        public string Nombre { get; set; }

        [Required (ErrorMessage = "El campo Apellido Paterno es requerido")]
        [Display(Name = "Apellido Paterno")]
        public string ApellidoPaterno { get; set; }

        [Required (ErrorMessage = "El campo Apellido Materno es requerido")]
        [Display(Name = "Apellido Materno")]
        public string ApellidoMaterno { get; set; }
    }
}
